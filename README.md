# Lab7 - Intro to Rust (Rust)


----------------
### Prelim. info

*Crates used for this program:*

- none


### Task 1: Acronym generator
> - Implement the function acronym to return the capitalised acronym of a given text sequence.
> -    Capital letters should stay capitalised, however, if there is an acronym in the phrase, only the first letter should be used.
> -    First letter of words should be used, and capitalised.
> -   Whitespace characters, tabs, underscore, vertical bar | and dash _ should be treated as word separators.

--------------
### Task 2: Reverse a string
```
Implement the function reverse to return the reversed input string.
```

--------------
### Task 2: Bird watching
Go over a list of bird IDs as strings and create the following functions for them
#### find_most_frequent_bird_count: 
> - checks which birds were the most frequently sighted and reports the total count
#### find_most_frequent_bird_no_order:
> - This function, goes over the log data and checks which birds were the most sighted, in case of a tie the implementation just picks one
#### find_most_frequent_bird:
> - Same as above, but this time, in case of a tie, in case there are more than a single bird with the same number of sightings,
    the bird that was observed first must be returned.

### Result
The functions pass all tests that were set up as part of the assignment template