// Lab 07
use std::collections::HashMap;
fn main() {
    test_acronym();
    
    test_reverse();

    test_find_most_frequent_bird_count();
    test_find_most_frequent_bird(find_most_frequent_bird);
    test_find_most_frequent_bird(find_most_frequent_bird_no_order);

    test_find_most_frequent_bird_with_order();
    test_find_most_frequent_bird_no_order()
}


fn reverse(input: &str) -> String {
    // Creating return string
    let mut reversed = String::new();
    // Returning an iterator over the chars of input string slice
    let mut chars = input.chars();
    // Iterating over the chars
    for _ in input.chars(){
        // Removing the last char from the iterator and pushing it to the return string
        reversed.push(chars.next_back().unwrap());
    }
    // Returning the reversed string
    reversed
}


fn acronym(phrase: &str) -> String {
    // Splitting the phrase into words according to task requirements
    phrase.split(|c: char| c.is_whitespace() || c == '_' || c == '-' || c == '|')
        .filter(|word| !word.is_empty()) // Filtering out any empty results from split to avoid processing them
        .map(|word| {
            // Converting each word into characters, enumerating them to keep track of indices
            word.chars().enumerate()
                .filter(|&(i, c)| {
                    if i == 0 {
                        true // Always include the first character of each word
                    } else {
                        // For subsequent characters, include them if they are uppercase and the previous character is not uppercase
                        c.is_uppercase() && !word.chars().nth(i - 1).unwrap().is_uppercase()
                    }
                })
                // Mapping to get characters: capitalize the first character, take others as they are if they pass the filter
                .map(|(i, c)| if i == 0 { c.to_ascii_uppercase() } else { c })
                // Collecting the filtered and mapped characters into a String for each word
                .collect::<String>()
        })
        // Collecting all the processed words into a single String to form the final acronym
        .collect()
}


fn find_most_frequent_bird_count(birds: &Vec<&str>) -> u64 {
    // Initialize a HashMap to count occurrences of each bird
    let mut counts = HashMap::new();
    // Loop through each bird in the vector
    for &bird in birds {
        // Increment the count for the bird, or insert 0 and then increment if not already present
        *counts.entry(bird).or_insert(0) += 1;
    }
    // Find and return the maximum count as u64, or return 0 if the vector is empty
    *counts.values().max().unwrap_or(&0) as u64
}


fn find_most_frequent_bird_no_order<'a>(birds: &[&'a str]) -> Option<&'a str> {
    // Initialize a HashMap to count occurrences of each bird
    let mut counts = HashMap::new();
    // Loop through each bird in the slice
    for &bird in birds {
        // Increment the count for the bird, or insert 0 and then increment if not already present
        *counts.entry(bird).or_insert(0) += 1;
    }
    // Find the bird with the maximum count and return it, or None if the slice is empty
    counts.iter().max_by_key(|(_, &count)| count).map(|(&bird, _)| bird)
}


fn find_most_frequent_bird<'a>(birds: &[&'a str]) -> Option<&'a str> {
    // Initialize a HashMap to count occurrences of each bird
    let mut counts = HashMap::new();
    // Initialize a HashMap to record the first sighting index of each bird
    let mut first_seen = HashMap::new();

    // Loop over the birds array with indices
    for (index, &bird) in birds.iter().enumerate() {
        // Increment the count for the bird, or insert 0 and then increment if not already present
        let count = counts.entry(bird).or_insert(0);
        *count += 1;

        // Record the first index at which each bird is seen, only if it hasn't been recorded yet
        first_seen.entry(bird).or_insert(index);
    }

    // Variables to keep track of the most frequently seen bird and its first sighting index
    let mut result_bird = None;
    let mut max_count = 0;
    let mut earliest_index = usize::MAX;

    // Iterate over the count entries to determine the most frequent bird
    for (&bird, &count) in counts.iter() {
        let index = first_seen[&bird];
        // Check if the current bird has a higher count or if it's a tie and the current bird was seen earlier
        if count > max_count || (count == max_count && index < earliest_index) {
            // Update the most frequent bird and its sighting details
            max_count = count;
            earliest_index = index;
            result_bird = Some(bird);
        }
    }

    // Return the bird that was most frequently seen or None if there are no birds
    result_bird
}




/////////////////////////////// Tests //////////////////////////////////
// Run the tests with `cargo run --release` to see if everything worked.


fn test_acronym() {
    let data = vec![
        ("Lecturer's like to use acronyms in their lectures", "LLTUAITL"),
        ("Lecturer's like-to_use|acronyms in their\tlectures", "LLTUAITL"),
        ("Portable Network Graphics", "PNG"),
        ("GNU Image Manipulation Program", "GIMP"),
        ("GNU Image Manipulation Program", "GIMP"),
        ("Rolling On The Floor Laughing So Hard", "ROTFLSH"),
        ("Ruby on Rails", "ROR"),
        ("HyperText Markup Language", "HTML"),
        ("First In, First Out", "FIFO"),
        ("PHP: Hypertext Preprocessor", "PHP"),
        ("PNG: Network Graphics", "PNG"),
        ("Make IT easy", "MIE"),
        ("Make I:T easy", "MITE"),
        ("Make I'T easy", "MITE"),
        ("Complementary metal-oxide semiconductor", "CMOS"),
        ("Complementary:metal-oxide semiconductor", "COS"),
    ];
    for (input, expected) in data {
        let output = acronym(input);
        assert_eq!(output, expected);
    }
}


fn test_reverse() {
    let data = [ 
                ("", ""), ("a", "a"), // edge cases
                ("Hello", "olleH"), ("World", "dlroW"), // Hellow World, of course :)
                ("1234567890", "0987654321"), ("123456789", "987654321"),
                ("This is my string", "gnirts ym si sihT"),
                ("This\tis my\n string", "gnirts \nym si\tsihT"), // with tabs and newlines
                ];
    for (input, expected) in data {
        let output = reverse(input);
        assert_eq!(output, expected);
    }
}


fn test_find_most_frequent_bird_count() {
    let data = [
        (vec!("a1","bz2","a3","a1","bz2","a1"), 3 as u64),
        (vec!("a1","bz2","a3","a1","bz2","a1","a1"), 4),
        (vec!("a1","bz2","a3","a1","bz2","a1","a1","a1"), 5),
        (vec!("a1","bz2","a3","a1","bz2","a1","a1","a1","a1"), 6),
        (vec!("a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1"), 7),
        (vec!("a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1","a1"), 8),
        (vec!("a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1","a1","a1"), 9),
        (vec!("a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1","a1","a1","a1"), 10),
        (vec!("a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1","a1","a1","a1","a1"), 11),
        (vec!("a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1","a1","a1","a1","a1","a1"), 12)];
    for (input, expected) in data {
        let output = find_most_frequent_bird_count(&input);
        assert_eq!(output, expected);
    }
}


// Normal test cases that are independent on the order of the most frequent bird appearance
// in the data log.
fn test_find_most_frequent_bird(f: for<'a> fn(&[&'a str]) -> Option<&'a str>){
    let data = [
        (vec!(), None), // edge case
        (vec!("a1"), Some("a1")), // edge case
        (vec!["a1","bz2","a3","a1","bz2","a1"], Some("a1")),
        (vec!["a1","bz2","a3","a1","bz2","a1","a1"], Some("a1")),
        (vec!["a1","bz2","a3","a1","bz2","a1","a1","a1"], Some("a1")),
        (vec!["a1","bz2","a3","a1","bz2","a1","a1","a1","a1"], Some("a1")),
        (vec!["a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1"], Some("a1")),
        (vec!["a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1","a1"], Some("a1")),
        (vec!["a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1","a1","a1"], Some("a1")),
        (vec!["a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1","a1","a1","a1"], Some("a1")),
        (vec!["a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1","a1","a1","a1","a1"], Some("a1")),
        (vec!["a1","bz2","a3","a1","bz2","a1","a1","a1","a1","a1","a1","a1","a1","a1","a1"], Some("a1")),
        ];

    for (input, expected) in data {
        let output = f(&input);
        assert_eq!(output, expected);
    }
}

// Test cases that are dependent on the order of the most frequent bird appearance
// in the data log. These test if the spec has been completed properly. 
fn test_find_most_frequent_bird_with_order() {
    let data = [
        (vec!(), None), // edge case
        (vec!("a1"), Some("a1")), // edge case
        (vec!("a1","a2","a3"), Some("a1")), // edge case, must return the first most frequent
        (vec!("a1","a2","a3","a1","a2","a3"), Some("a1")), // edge case, must return the first most frequent
        (vec!("a2","a1","a1","a2","a2","a3"), Some("a2")),
        (vec!("a1","a2","a2","a1","a2","a1"), Some("a1")), 
        (vec!("a1","a2","a3","a3","a3","a2","a2"), Some("a2")) // edge case, must return the first most frequent
        ];

    for (input, expected) in data {
        let output = find_most_frequent_bird(&input);
        println!("{:?} is in {:?}", output, expected);
        assert_eq!(output, expected);
    }
}


fn test_find_most_frequent_bird_no_order() {
    let data = [
        (vec!(), vec!(None)), // edge case
        (vec!("a1"), vec!(Some("a1"))), // edge case
        (vec!("a1","a2","a3"), vec!(Some("a1"), Some("a2"), Some("a3"))), // edge case, must return the first most frequent
        (vec!("a1","a2","a3","a1","a2","a3"), vec!(Some("a1"), Some("a2"), Some("a3"))), // edge case, must return the first most frequent
        (vec!("a2","a1","a1","a2","a2","a3"), vec!(Some("a2"))),
        (vec!("a1","a2","a2","a1","a2","a1"), vec!(Some("a1"), Some("a2"))), 
        (vec!("a1","a2","a3","a3","a3","a2","a2"), vec!(Some("a2"), Some("a3"))) // edge case, must return the first most frequent
        ];

    for (input, expected) in data {
        let output = find_most_frequent_bird_no_order(&input);
        // Debugging output in case we make an error in test cases
        // println!("{:?} is in {:?}", output, expected);
        assert!(expected.contains(&output));
    }
}


